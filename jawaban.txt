1. Membuat database

CREATE DATABASE myshop;

2. membuat table

users
CREATE TABLE users ( id int(6) PRIMARY KEY AUTO_INCREMENT, name varchar(255), email varchar(255), password varchar(255) );

categories
CREATE TABLE categories( id int(6) PRIMARY KEY AUTO_INCREMENT, name varchar(255) );

items
CREATE TABLE items ( id int(6) PRIMARY KEY AUTO_INCREMENT, name varchar(255), description varchar(255), price int(11), stock int (4), categories_id int(6), FOREIGN KEY(categories_id) REFERENCES categories(id) );


3. Memasukkan data

users
INSERT INTO users (name, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "Jane@doe.com", "jane123");

categories
INSERT INTO categories (name) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

items
INSERT INTO items(name, description, price, stock, categories_id) VALUES ("sumsang b50","hape keren dari merek sumsang",4000000,100,1), ("uniklooh","baju keren dari brand ternama",500000,50,2), ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);


4. Mengambil data dari database

data users
SELECT name, email FROM `users`;

data items
	diatas 1jt
	SELECT * FROM `items` WHERE price >1000000;

	LIKE
	SELECT * FROM `items` WHERE name LIKE "%sang%";

join
SELECT items.name, items.description, items.price, items.stock, items.categories_id, categories.name AS kategori FROM `items` INNER JOIN categories ON items.categories_id = categories.id;


5. mengubah data dari database
UPDATE items SET price = 2500000 WHERE name="sumsang b50";